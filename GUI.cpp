#include <gtk/gtk.h>
#include <stdio.h>
#include "tools.h"
#include "GUI.h"
#include "x86_tool.h"
#include "odroid_status.h"
#include <sys/ipc.h>
#include <sys/shm.h>
#include <sys/types.h>
#include <signal.h>
#include <netinet/in.h>
#include <netdb.h> 

#define DATA_TAG_NUM 4
#define POWER_AREA_FIXED 100

GdkPixbuf *pix1, *pix2, *pix3;
GtkWidget *drawing_area1, *drawing_area2, *drawing_area3;
GtkWidget *power_area;
GtkWidget *FPS;
GtkWidget *Stream_speed;

extern int 			shmimg_id;
extern int 			shmsync_id;
extern img_meta 	*img_meta_ptr;
extern void			*img_data1, *img_data2, *img_data3;
extern int 			childpid;
extern uint32_t  	*newsetting;
extern data_fmt 	*GUI_data;

int sockfd;
GdkPoint data_cpu[NUM_OF_DATA];

int get_data(int datatype){
	int status_cpu = datatype;
	int ret;
	ewrite(sockfd, &status_cpu, 4);
	eread(sockfd, &ret, 4);
	// fprintf(stderr, "%d %d\n",datatype,ret );
	return ret*POWER_HEIGHT/SCALE/4;
}
void initdata(GdkPoint *data){
	int i;
	for(i=0; i<NUM_OF_DATA; i++){
		data[i].x = i*POWER_RES;
		data[i].y = POWER_HEIGHT;
	}
}
/*
int settingdata(GdkPoint *data, int datatype, int setthis){
	int i, got = 0;
	for(i=0; i<NUM_OF_DATA-1; i++){
		data[i].y = data[i+1].y;
	}
	switch(datatype){
		case STATUS_CPU:
		case STATUS_POWER_A15:
		case STATUS_POWER_MEM:
		case STATUS_POWER_A7:
		case STATUS_POWER_GPU:
			got = get_data(datatype);
			break;
		case STATUS_POWER_TOTAL:
			got = setthis;
			break;
	}
	data[NUM_OF_DATA-1].y = POWER_HEIGHT - got;
	return got;
}
*/
static gboolean power_area_expose_event(GtkWidget      *widget,
		GdkEventExpose *event,
		gpointer        user_data ) {
	GdkWindow *window;
	GdkGC *gc;
	GdkColor col;
	col.pixel = 0x0000;
	int i;

	window = widget->window;
	gc = gdk_gc_new (window);


	col.red = 0xffff;
	col.green = 0xffff;
	col.blue = 0xffff;
	gdk_gc_set_rgb_fg_color (gc, &col);
	gdk_gc_set_line_attributes (gc, 3, GDK_LINE_SOLID, GDK_CAP_ROUND, GDK_JOIN_ROUND);
	gdk_draw_rectangle(window, gc, TRUE, 0, 0, POWER_WIDTH, 170);


	col.red = 50720;
	col.green = 50720;
	col.blue = 50720;
	gdk_gc_set_rgb_fg_color (gc, &col);
	gdk_gc_set_line_attributes (gc, 1, GDK_LINE_ON_OFF_DASH, GDK_CAP_ROUND, GDK_JOIN_ROUND);
	for(i=1;i<DATA_TAG_NUM;i++)
 		gdk_draw_line(window, gc, 0, POWER_HEIGHT*i/DATA_TAG_NUM, POWER_WIDTH, POWER_HEIGHT*i/DATA_TAG_NUM);

 	/*draw data*/
	col.red = 0xf700;
	col.green = 0x8000;
	col.blue = 0x4000;
	gdk_gc_set_rgb_fg_color (gc, &col);
	gdk_gc_set_line_attributes (gc, 3, GDK_LINE_SOLID, GDK_CAP_ROUND, GDK_JOIN_ROUND);
	gdk_draw_lines (window,	gc,	data_cpu, NUM_OF_DATA);

	col.red = 30720;
	col.green = 30720;
	col.blue = 30720;
	gdk_gc_set_rgb_fg_color (gc, &col);
	gdk_gc_set_line_attributes (gc, 2, GDK_LINE_SOLID, GDK_CAP_ROUND, GDK_JOIN_ROUND);
 	gdk_draw_line(window, gc, 1, 1, POWER_WIDTH+1, 1);
 	gdk_draw_line(window, gc, 1, 1, 1, POWER_HEIGHT+1);
 	gdk_draw_line(window, gc, POWER_WIDTH+1, POWER_HEIGHT+1, POWER_WIDTH+1, 1);
 	gdk_draw_line(window, gc, POWER_WIDTH+1, POWER_HEIGHT+1, 1, POWER_HEIGHT+1);


	gdk_gc_unref (gc);
	return TRUE;
}

// GdkPixbuf *create_pixbuf1(const gchar * filename)
// {
// 	GdkPixbuf *pixbuf;
// 	GError *error = NULL;
// 	pixbuf = gdk_pixbuf_new_from_file(filename, &error);
// 	if(!pixbuf) {
// 		fprintf(stderr, "%s\n", error->message);
// 		g_error_free(error);
// 	}

// 	return pixbuf;
// }

static gboolean time_handler( GtkWidget *widget ) {
	char buf[80];
	sprintf(buf,"<span foreground='red' size='x-large'>%.1f FPS</span>", img_meta_ptr->FPS);
	gtk_label_set_markup(GTK_LABEL(FPS), buf);
	sprintf(buf,"<span foreground='green' size='x-large'>%lld KB/S</span>", img_meta_ptr->Stream_speed);
	gtk_label_set_markup(GTK_LABEL(Stream_speed), buf);
//	settingdata(data_cpu, STATUS_CPU, 0);
	
//	gtk_widget_queue_draw(GTK_WIDGET(power_area));
	gtk_widget_queue_draw( GTK_WIDGET( widget ));
	return TRUE;
}

void callback_480P(GtkWidget *button, gpointer data) {
//	if(GUI_data->img_width != 640){
//		while(xchg(newsetting, 1));
//		GUI_data->img_width = 640;
//		GUI_data->img_height = 480;
//	}
}
//void callback_720P(GtkWidget *button, gpointer data) {
//	if(GUI_data->img_width != 1280){
//		while(xchg(newsetting, 1));
//		GUI_data->img_width = 1280;
//		GUI_data->img_height = 720;
//	}
//}
void callback_Non_det(GtkWidget *button, gpointer data) {
	if(GUI_data->proc_dev != NO_PROC){
		while(xchg(newsetting, 1));
		GUI_data->proc_dev = NO_PROC;
	}
}
void callback_Remote(GtkWidget *button, gpointer data) {
	if(GUI_data->proc_dev != REMOTE_PROC){
		while(xchg(newsetting, 1));
		GUI_data->proc_dev = REMOTE_PROC;
	}
}
void callback_Local(GtkWidget *button, gpointer data) {
	if(GUI_data->proc_dev != LOCAL_PROC){
		while(xchg(newsetting, 1));
		GUI_data->proc_dev = LOCAL_PROC;
	}
}
void callback_algo_face(GtkWidget *button, gpointer data) {
//	if(GUI_data->algorithm != ALGO_FACE){
//		while(xchg(newsetting, 1));
//		GUI_data->algorithm = ALGO_FACE;
//	}
}
//void callback_algo_pd(GtkWidget *button, gpointer data) {
//	if(GUI_data->algorithm != ALGO_PD){
//		while(xchg(newsetting, 1));
//		GUI_data->algorithm = ALGO_PD;
//	}
//}
gboolean expose_event_callback1( GtkWidget      *widget,
		GdkEventExpose *event,
		gpointer        data) {
	gtk_widget_set_size_request( drawing_area1, 560, 420 );
//	gtk_widget_set_size_request( drawing_area2, 400, 300 );
//	gtk_widget_set_size_request( drawing_area3, 400, 300 );

	pix1 = gdk_pixbuf_new_from_data(( guchar* )img_data1,
			GDK_COLORSPACE_RGB,
			FALSE,
			img_meta_ptr->depth,
			img_meta_ptr->width,
			img_meta_ptr->height,
			( img_meta_ptr->width * img_meta_ptr->channels ),
			NULL, NULL);
//	gdk_pixbuf_scale_simple(pix1, 560, 420, GDK_INTERP_BILINEAR); 
	
	gdk_draw_pixbuf( widget->window,
			widget->style->fg_gc[ GTK_WIDGET_STATE( widget )],
			pix1,
			0, 0, 0, 0,
			560,
			420,
			GDK_RGB_DITHER_MAX,
			0, 0);
/*        gdk_draw_pixbuf( widget->window,
                        widget->style->fg_gc[ GTK_WIDGET_STATE( widget )],
                        pix2,
                        0, 0, 0, 0,
                        img_meta_ptr->width/2,
                        img_meta_ptr->height/2,
                        GDK_RGB_DITHER_MAX,
                        0, 0);
        gdk_draw_pixbuf( widget->window,
                        widget->style->fg_gc[ GTK_WIDGET_STATE( widget )],
                        pix3,
                        0, 0, 0, 0,
                        img_meta_ptr->width/2,
                        img_meta_ptr->height/2,
                        GDK_RGB_DITHER_MAX,
                        0, 0);
*/
	return TRUE;
}

gboolean expose_event_callback2( GtkWidget      *widget,
                GdkEventExpose *event,
                gpointer        data) {
//      gtk_widget_set_size_request( drawing_area1, 400, 300 );
     	gtk_widget_set_size_request( drawing_area2, 560, 420 );
//      gtk_widget_set_size_request( drawing_area3, 400, 300 );

        pix2 = gdk_pixbuf_new_from_data(( guchar* )img_data2,
                        GDK_COLORSPACE_RGB,
                        FALSE,
                        img_meta_ptr->depth,
                        img_meta_ptr->width,
                        img_meta_ptr->height,
                        ( img_meta_ptr->width * img_meta_ptr->channels ),
                        NULL, NULL);
//        pix2 = gdk_pixbuf_scale_simple(pix1, 560, 420, GDK_INTERP_BILINEAR);

        gdk_draw_pixbuf( widget->window,
                        widget->style->fg_gc[ GTK_WIDGET_STATE( widget )],
                        pix2,
                        0, 0, 0, 0,
                        560,
                        420,
                        GDK_RGB_DITHER_MAX,
                        0, 0);
  //      g_object_unref(pix2);
//        g_object_unref(pix2_resize);
/*
        gdk_draw_pixbuf( widget->window,
                        widget->style->fg_gc[ GTK_WIDGET_STATE( widget )],
                        pix2,
                        0, 0, 0, 0,
                        img_meta_ptr->width/2,
                        img_meta_ptr->height/2,
                        GDK_RGB_DITHER_MAX,
                        0, 0);
        gdk_draw_pixbuf( widget->window,
                        widget->style->fg_gc[ GTK_WIDGET_STATE( widget )],
                        pix3,
                        0, 0, 0, 0,
                        img_meta_ptr->width/2,
                        img_meta_ptr->height/2,
                        GDK_RGB_DITHER_MAX,
                        0, 0);
*/
        return TRUE;
}

gboolean expose_event_callback3( GtkWidget      *widget,
                GdkEventExpose *event,
                gpointer        data) {
//        gtk_widget_set_size_request( drawing_area1, 400, 300 );
//      gtk_widget_set_size_request( drawing_area2, 400, 300 );
     	gtk_widget_set_size_request( drawing_area3, 560, 420 );

        pix3 = gdk_pixbuf_new_from_data(( guchar* )img_data3,
                        GDK_COLORSPACE_RGB,
                        FALSE,
                        img_meta_ptr->depth,
                        img_meta_ptr->width,
                        img_meta_ptr->height,
                        ( img_meta_ptr->width * img_meta_ptr->channels ),
                        NULL, NULL);
//	pix3 = gdk_pixbuf_scale_simple(pix1, 560, 420, GDK_INTERP_BILINEAR);

        gdk_draw_pixbuf( widget->window,
                        widget->style->fg_gc[ GTK_WIDGET_STATE( widget )],
                        pix3,
                        0, 0, 0, 0,
                        560,
                        420,
                        GDK_RGB_DITHER_MAX,
                        0, 0);
    //    g_object_unref(pix3);
  //      g_object_unref(pix3_resize);
/*
        gdk_draw_pixbuf( widget->window,
                        widget->style->fg_gc[ GTK_WIDGET_STATE( widget )],
                        pix2,
                        0, 0, 0, 0,
                        img_meta_ptr->width/2,
                        img_meta_ptr->height/2,
                        GDK_RGB_DITHER_MAX,
                        0, 0);
        gdk_draw_pixbuf( widget->window,
                        widget->style->fg_gc[ GTK_WIDGET_STATE( widget )],
                        pix3,
                        0, 0, 0, 0,
                        img_meta_ptr->width/2,
                        img_meta_ptr->height/2,
                        GDK_RGB_DITHER_MAX,
                        0, 0);
*/
        return TRUE;
}


GtkWidget* Buttons() {
	GtkWidget *fr_radio1, *fr_radio2, *fd_radio1, *fd_radio2, *fd_radio3, *fa_radio1, *fa_radio2,
		  *hbox_total, *vbox_left, *vbox_right, *hbox, *fr_hbox, *fd_hbox, *fa_hbox,
		  *frame_resolution, *frame_detectdev, *frame_algorithm, *frame_FPS, *frame_Stream_speed;

	fr_radio1 = gtk_radio_button_new_with_label(NULL, "480P");
//	fr_radio2 = gtk_radio_button_new_with_label_from_widget(GTK_RADIO_BUTTON(fr_radio1) , "720P");
//	frame_resolution = gtk_frame_new("Frame resolution");
	fr_hbox = gtk_hbutton_box_new();
	gtk_button_box_set_layout(GTK_BUTTON_BOX(fr_hbox), GTK_BUTTONBOX_SPREAD);
//	gtk_container_add(GTK_CONTAINER(frame_resolution), fr_hbox);
	g_signal_connect(GTK_OBJECT(fr_radio1), "pressed", G_CALLBACK(callback_480P), NULL);
//	g_signal_connect(GTK_OBJECT(fr_radio2), "pressed", G_CALLBACK(callback_720P), NULL);
	gtk_box_pack_start(GTK_BOX(fr_hbox), fr_radio1, TRUE, TRUE, 5);
//	gtk_box_pack_start(GTK_BOX(fr_hbox), fr_radio2, TRUE, TRUE, 5);

	fd_radio1 = gtk_radio_button_new_with_label(NULL, "Non-detecting");
	fd_radio2 = gtk_radio_button_new_with_label_from_widget(GTK_RADIO_BUTTON(fd_radio1) , "Remote");
	fd_radio3 = gtk_radio_button_new_with_label_from_widget(GTK_RADIO_BUTTON(fd_radio1) , "Local");
	frame_detectdev = gtk_frame_new("Detecting device");
	fd_hbox = gtk_hbutton_box_new();
	gtk_button_box_set_layout(GTK_BUTTON_BOX(fd_hbox), GTK_BUTTONBOX_SPREAD);
	gtk_container_add(GTK_CONTAINER(frame_detectdev), fd_hbox);
	g_signal_connect(GTK_OBJECT(fd_radio1), "pressed", G_CALLBACK(callback_Non_det), NULL);
	g_signal_connect(GTK_OBJECT(fd_radio2), "pressed", G_CALLBACK(callback_Remote), NULL);
	g_signal_connect(GTK_OBJECT(fd_radio3), "pressed", G_CALLBACK(callback_Local), NULL);
	gtk_box_pack_start(GTK_BOX(fd_hbox), fd_radio1, TRUE, TRUE, 5);
	gtk_box_pack_start(GTK_BOX(fd_hbox), fd_radio2, TRUE, TRUE, 5);
	gtk_box_pack_start(GTK_BOX(fd_hbox), fd_radio3, TRUE, TRUE, 5);

	fa_radio1 = gtk_radio_button_new_with_label(NULL, "Face Detection");
//	fa_radio2 = gtk_radio_button_new_with_label_from_widget(GTK_RADIO_BUTTON(fa_radio1) , "People Counting");
	//frame_algorithm = gtk_frame_new("Algorithm");
	fa_hbox = gtk_hbutton_box_new();
	gtk_button_box_set_layout(GTK_BUTTON_BOX(fa_hbox), GTK_BUTTONBOX_SPREAD);
//	gtk_container_add(GTK_CONTAINER(frame_algorithm), fa_hbox);
	g_signal_connect(GTK_OBJECT(fa_radio1), "pressed", G_CALLBACK(callback_algo_face), NULL);
//	g_signal_connect(GTK_OBJECT(fa_radio2), "pressed", G_CALLBACK(callback_algo_pd), NULL);
	gtk_box_pack_start(GTK_BOX(fa_hbox), fa_radio1, TRUE, TRUE, 5);
//	gtk_box_pack_start(GTK_BOX(fa_hbox), fa_radio2, TRUE, TRUE, 5);

	frame_FPS = gtk_frame_new("FPS");
	FPS  = gtk_label_new(NULL);
	gtk_container_add(GTK_CONTAINER(frame_FPS), FPS);

	frame_Stream_speed = gtk_frame_new("Streaming speed");
	Stream_speed = gtk_label_new(NULL);
	gtk_container_add(GTK_CONTAINER(frame_Stream_speed), Stream_speed);

	hbox = gtk_hbox_new(FALSE, 5);
//	gtk_box_pack_start(GTK_BOX(hbox), frame_resolution, TRUE, TRUE, 0);
//	gtk_box_pack_start(GTK_BOX(hbox), frame_algorithm, TRUE, TRUE, 0);

	vbox_left = gtk_vbox_new(FALSE, 5);
	gtk_box_pack_start(GTK_BOX(vbox_left), hbox, TRUE, TRUE, 0);
	gtk_box_pack_start(GTK_BOX(vbox_left), frame_detectdev, TRUE, TRUE, 0);

	vbox_right = gtk_hbox_new(FALSE, 5);
	gtk_box_pack_start(GTK_BOX(vbox_right), frame_FPS, TRUE, TRUE, 0);
	gtk_box_pack_start(GTK_BOX(vbox_right), frame_Stream_speed, TRUE, TRUE, 0);	

	hbox_total = gtk_hbox_new(FALSE, 0);
	gtk_box_pack_start(GTK_BOX(hbox_total), vbox_left, TRUE, TRUE, 5);
	gtk_box_pack_start(GTK_BOX(hbox_total), vbox_right, TRUE, TRUE, 5);	

	return hbox_total;
}

static void window_destroy(GtkWidget *widget, gpointer data){
	shmctl(shmimg_id, IPC_RMID, NULL);
	shmctl(shmsync_id, IPC_RMID, NULL);
	kill(childpid, SIGTERM);
	gtk_main_quit();
}


int gui_main( int argc, char *argv[] ) {
	GtkWidget *window;
	GtkWidget *vbox, *hbox_back_cam;
	GtkWidget *alignment1, *alignment2, *alignment3, *power_alignment;
	GtkWidget *fixed;
	GtkWidget *label0, *label1, *label2, *label3, *label4/*, *label5*/
	;

	initdata(data_cpu);
	struct hostent *server = gethostbyname(argv[1]);
	if(!server){
		error("gethostbyname error\n");
	}
//	sockfd = sock_init(server, STATUS_PORT);

	/*initial shared memory ptr*/
	img_meta_ptr = (img_meta*)shmat(shmimg_id, NULL, SHM_RDONLY ) ;
	img_data1 = img_meta_ptr + 1;
	img_data2 = img_data1 + 560*420*3;
	img_data3 = img_data2 + 560*420*3;
	newsetting = (uint32_t*)shmat(shmsync_id, NULL, 0);
	GUI_data = (data_fmt *)(newsetting + 1);

	gtk_init (&argc, &argv);
	window = gtk_window_new(GTK_WINDOW_TOPLEVEL);

	gtk_window_set_title( GTK_WINDOW( window ), "WEBCAM");
	// gtk_window_set_icon(GTK_WINDOW(window), create_pixbuf1("logo.png"));
	gtk_window_set_default_size(GTK_WINDOW(window), 1140, 900);
	g_signal_connect( G_OBJECT( window ), "destroy", G_CALLBACK (window_destroy), NULL);
	vbox = gtk_vbox_new(FALSE, 5);
	hbox_back_cam = gtk_hbox_new(FALSE, 5);

	alignment1 = gtk_alignment_new(0.5, 0, 0, 0);
	gtk_widget_set_size_request (alignment1, 560, 420);
	gtk_container_set_border_width (GTK_CONTAINER (window), 10);

        alignment2 = gtk_alignment_new(1, 1, 0, 0);
        gtk_widget_set_size_request (alignment1, 560, 420);
        gtk_container_set_border_width (GTK_CONTAINER (window), 10);

        alignment3 = gtk_alignment_new(0, 1, 0, 0);
        gtk_widget_set_size_request (alignment1, 560, 420);
        gtk_container_set_border_width (GTK_CONTAINER (window), 10);


	drawing_area1 = gtk_drawing_area_new();
	gtk_widget_set_size_request( drawing_area1, 560, 420 );//1280 720
	g_signal_connect( G_OBJECT( drawing_area1), "expose_event", G_CALLBACK (expose_event_callback1), NULL);
	gtk_container_add(GTK_CONTAINER(alignment1), drawing_area1);

	drawing_area2 = gtk_drawing_area_new();
        gtk_widget_set_size_request( drawing_area2, 560, 420 );//1280 720
        g_signal_connect( G_OBJECT( drawing_area2), "expose_event", G_CALLBACK (expose_event_callback2), NULL);
        gtk_container_add(GTK_CONTAINER(alignment2), drawing_area2);

	drawing_area3 = gtk_drawing_area_new();
        gtk_widget_set_size_request( drawing_area3, 560, 420 );//1280 720
        g_signal_connect( G_OBJECT( drawing_area3), "expose_event", G_CALLBACK (expose_event_callback3), NULL);
        gtk_container_add(GTK_CONTAINER(alignment3), drawing_area3);
/*
	power_area = gtk_drawing_area_new ();
	gtk_widget_set_size_request (power_area, POWER_WIDTH+3, POWER_HEIGHT+3);
	g_signal_connect (power_area, "expose-event", G_CALLBACK (power_area_expose_event), NULL);
	fixed = gtk_fixed_new();
	gtk_fixed_put(GTK_FIXED(fixed), power_area, POWER_AREA_FIXED, 8);
*/
/*	
	label0 = gtk_label_new("0 W");
	label1 = gtk_label_new("1 W");
	label2 = gtk_label_new("2 W");
	label3 = gtk_label_new("3 W");
	label4 = gtk_label_new("4 W");

	gtk_fixed_put(GTK_FIXED(fixed), label4, POWER_WIDTH+10 +POWER_AREA_FIXED, POWER_HEIGHT*0/DATA_TAG_NUM);
	gtk_fixed_put(GTK_FIXED(fixed), label3, POWER_WIDTH+10 +POWER_AREA_FIXED, POWER_HEIGHT*1/DATA_TAG_NUM);
	gtk_fixed_put(GTK_FIXED(fixed), label2, POWER_WIDTH+10 +POWER_AREA_FIXED, POWER_HEIGHT*2/DATA_TAG_NUM);
	gtk_fixed_put(GTK_FIXED(fixed), label1, POWER_WIDTH+10 +POWER_AREA_FIXED, POWER_HEIGHT*3/DATA_TAG_NUM);
	gtk_fixed_put(GTK_FIXED(fixed), label0, POWER_WIDTH+10 +POWER_AREA_FIXED, POWER_HEIGHT);

	power_alignment = gtk_alignment_new(1, 1, 0.5, 0.5);
	gtk_container_add(GTK_CONTAINER(power_alignment), fixed);
*/
	//gtk_box_pack_start(GTK_BOX(hbox), alignment1, FALSE, FALSE, 0);
	gtk_box_pack_start(GTK_BOX(hbox_back_cam), alignment2, FALSE, FALSE, 0);
	gtk_box_pack_start(GTK_BOX(hbox_back_cam), alignment3, FALSE, FALSE, 0);
	gtk_box_pack_start(GTK_BOX(vbox), alignment1, FALSE, FALSE, 0);
	gtk_box_pack_start(GTK_BOX(vbox), hbox_back_cam, FALSE, FALSE, 0);
//	gtk_box_pack_start(GTK_BOX(vbox), power_alignment, FALSE, FALSE, 0);
	gtk_box_pack_start(GTK_BOX(vbox), Buttons(), FALSE, FALSE, 0);
	gtk_container_add(GTK_CONTAINER(window), vbox);
	g_timeout_add( 30, ( GSourceFunc )time_handler, ( gpointer )drawing_area1 );
	g_timeout_add( 30, ( GSourceFunc )time_handler, ( gpointer )drawing_area2 );
	g_timeout_add( 30, ( GSourceFunc )time_handler, ( gpointer )drawing_area3 );
	gtk_widget_show_all(window);
	gtk_main();
	return 0;
}
