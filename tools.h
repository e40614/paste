#include <stdint.h>
//#include <cv.h>
#include <netinet/in.h>
#include <netdb.h> 

/* proc_dev */
#define REMOTE_PROC 1
#define LOCAL_PROC 2
#define NO_PROC 3

/* algorithm */
#define ALGO_FACE	1
#define ALGO_PD		2

#define INIT_WIDTH 640
#define INIT_HEIGHT 480

#define PROTNO 5000

#define min_face_height  100
#define min_face_width  100
#define max_face_height  500
#define max_face_width  500

#define XML_LOCATION "/usr/share/paste/haarcascade_frontalface_alt.xml"

void error(const char *msg);
void errno_exit(const char *s);
int eread(int fd, void *buf, size_t count);
int ewrite(int fd, const void *buf, size_t count);
int sock_init(struct hostent *server, int portno);

typedef struct data_fmt{
//	char camera_device[50];
	int camera_device[3];
	int proc_dev;
}data_fmt;

typedef struct img_meta{
	int width;
	int height;
	int depth;
	int channels;
	double FPS;
	long long Stream_speed;
}img_meta;

//finite state machine f=forward b=backward
enum sensor_stat {
        f=0,
        b
};

