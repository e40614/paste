#! /bin/sh
# /etc/init.d/paste.sh
#
 
# Some things that run always
 
# Carry out specific functions when asked to by the system
case "$1" in
  start)
    nohup /usr/bin/odroid_server 2>&1 >/dev/null &
    nohup /usr/bin/odroid_status 2>&1 >/dev/null &
    echo "paste start"
    ;;
  stop)
    kill `ps -C odroid_server -o pid=`
    kill `ps -C odroid_status -o pid=`
    echo "kill the process"
    ;;
  *)
    echo "Usage: /etc/init.d/paste.sh {start|stop}"
    exit 1
    ;;
esac
 
exit 0
