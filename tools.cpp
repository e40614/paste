#include <stdio.h>
#include <unistd.h>
#include <errno.h>
#include <string.h>
#include <stdlib.h>
#include "tools.h"

int eread(int fd, void *buf, size_t count){
	int n, ret = count;
	while( (n = read(fd, buf, count)) != count){
		if(n == 0){
			printf("read fd closed\n");
			return 0;
		}else if(n < 0){
			printf("ERROR read fd\n");
			return n;
		}
		buf += n;
		count -= n;
	}
	return ret;
}
int ewrite(int fd, const void *buf, size_t count){
	int n, ret = count;
	// fprintf(stderr,"K");
	// fprintf(stderr,"A%d C%d",write(fd, buf, count),count);
	while((n = write(fd, buf, count)) != count){
		// fprintf(stderr,"B");
		if(n == 0){
			fprintf(stderr,"write fd closed\n");
			return 0;
		}else if(n < 0){
			fprintf(stderr,"ERROR write fd\n");
			return n;
		}
		// fprintf(stderr,"C");
		buf += n;
		count -= n;
	}
	// fprintf(stderr,"%d ",n);
	return ret;
}

void errno_exit(const char *s)
{
	fprintf(stderr, "%s error %d, %s\n", s, errno, strerror(errno));
	exit(EXIT_FAILURE);
}

void error(const char *msg)
{
	perror(msg);
	exit(1);
}

int sock_init(struct hostent *server, int portno){
	struct sockaddr_in serv_addr;
	int netfd = socket(AF_INET, SOCK_STREAM, 0);
	/* open socket and connect to server */
	if (netfd < 0) 
		printf("ERROR opening socket");
	if (server == NULL) {
		fprintf(stderr,"ERROR, no such host\n");
		exit(0);
	}
	bzero((char *) &serv_addr, sizeof(serv_addr));
	serv_addr.sin_family = AF_INET;
	bcopy((char *)server->h_addr, (char *)&serv_addr.sin_addr.s_addr, server->h_length);
	serv_addr.sin_port = htons(portno);
	if (connect(netfd,(struct sockaddr *) &serv_addr,sizeof(serv_addr)) < 0) {
		printf("ERROR connecting to server\n");
		exit(-1);
	}
	return netfd;
}

