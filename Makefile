CC = g++
MACRO = ODROID
#use ODROID or PC for MACRO
CFLAGS = -O2 -Wall `pkg-config --cflags gtk+-2.0 opencv` -D$(MACRO)
# GUIFLAGS = $(CFLAGS) `pkg-config --cflags gtk+-2.0 opencv`
OCFLAGS = $(CFLAGS) `pkg-config --libs opencv`  -L/usr/local/lib 
PCFLAGS = $(CFLAGS) `pkg-config --libs opencv gtk+-2.0` -L/usr/local/lib

ODROID_OBJS = odroid.o
CMN_OBJS = I2Cdev.o MPU6050.o
HDRS = I2Cdev.h MPU6050.h

.PHONY:clean
	
all:odroid pc

$(CMN_OBJS) $(ODROID_OBJS)  : $(HDRS)

odroid:odroid.cpp tools.o $(CMN_OBJS) $(RAW_OBJS)
	$(CC) $^ -o $@ $(OCFLAGS)

pc:pc.cpp tools.o GUI.o 
	$(CC) $^ -o $@ $(PCFLAGS)

#odroid_status:odroid_status.cpp tools.o
#	$(CC) $^ $(CFLAGS) -o $@

%.o:%.cpp
	$(CC) $< $(CFLAGS) -c

install_all:paste.sh odroid odroid_status
	cp paste.sh /etc/init.d/
	chmod 755 /etc/init.d/paste.sh
	update-rc.d paste.sh defaults
	mkdir -p /usr/share/paste
	cp haarcascade_frontalface_alt.xml /usr/share/paste
	cp odroid /usr/bin/odroid_server
	cp odroid_status /usr/bin/

remove_all:
	update-rc.d paste remove
	rm /etc/init.d/paste.sh
	rm -r /usr/share/paste
	rm /usr/bin/odroid_server
	rm /usr/bin/odroid_status

install_basic:
	mkdir -p /usr/share/paste
	cp haarcascade_frontalface_alt.xml /usr/share/paste	
	cp data.xml /usr/share/paste
	cp ivs_PDS_demo_720P ivs_PDS_demo_720P_odroid ivs_PDS_demo_480P ivs_PDS_demo_480P_odroid /usr/bin/

remove_basic:
	rm -r /usr/share/paste
	rm -f /usr/bin/ivs_PDS_demo_720P /usr/bin/ivs_PDS_demo_720P_odroid /usr/bin/ivs_PDS_demo_480P /usr/bin/ivs_PDS_demo_480P_odroid


clean:
	rm -f *.o odroid pc odroid_status tags
