#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <string.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <netdb.h> 
#include <opencv2/core/core.hpp>
#include <opencv2/highgui/highgui.hpp>
//#include "cv.h"
#include "cxcore.h"
//#include "highgui.h"
#include <sys/time.h>
#include <unistd.h>// sleep(3);
#include <sys/timeb.h>//timeb
#include "tools.h"
#include "pc.h"
#include "x86_tool.h"
#include <sys/ipc.h>
#include <sys/shm.h>
#include <signal.h>

#include <iostream>
#include <highgui.h>
#include <cv.h>
#include <vector>
#include <opencv2/imgproc/imgproc.hpp>
using namespace std;
using namespace cv;

//vector<uchar> buff;//buffer for coding
vector<int> param = vector<int>(2);


extern int gui_main( int argc, char *argv[] );

data_fmt set_data;
data_fmt check_data;

int shmimg_id;
int shmsync_id;
uint32_t *newsetting;
data_fmt *GUI_data;
img_meta *img_meta_ptr;
void	 *img_data1, *img_data2, *img_data3;

int childpid;

CvMemStorage* facesMemStorage;
CvHaarClassifierCascade* classifier;



long long getSystemTime() {
	struct timeb t;
	ftime(&t);
	return 1000 * t.time + t.millitm;
}
void PrintFrameMsg(double fps,long long speed, char *fourcc){
	printf("\r");
	printf("[camera "LIGHT_RED"%s"CLOSE_COLOR YELLOW" %.3f"CLOSE_COLOR"FPS "YELLOW"%4lld"CLOSE_COLOR"KB/s]",fourcc, fps, speed);
	fflush(stdout);
}


void set_formate(int sockfd){
	ewrite(sockfd, set_data.camera_device, sizeof(int)*3);
	ewrite(sockfd, &(set_data.proc_dev),   sizeof(set_data.proc_dev));
}
int check_formate(int sockfd){
	eread(sockfd, check_data.camera_device, sizeof(int)*3);
	eread(sockfd, &(check_data.proc_dev),   sizeof(check_data.proc_dev));
/*
	if(check_data.camera_device !=  set_data.camera_device ||
		check_data.proc_dev   != set_data.proc_dev){
		printf("data formate setting different\n"
				"set_data.camera_device = %d\n"
			   "set_data.proc_dev = %d\n" ,set_data.camera_device, set_data.proc_dev);
		printf("data formate setting different\n"
				"check_data.camera_device = %d\n"
			   "check_data.proc_dev = %d\n" ,check_data.camera_device, check_data.proc_dev);
		return -1;
	}
*/
	return 0;
}

void change_setting(){

}

void shm_init(){
	void *ptr;
	img_meta *img_meta_ptr;
	shmimg_id = shmget(IPC_PRIVATE, sizeof(img_meta_ptr) + 560*420*3*3 , IPC_CREAT|0600 ) ;
	if ( shmimg_id < 0 )
	{
		perror("get shmimg_id ipc_id error") ;
		exit(-1) ;
	}
	ptr = shmat(shmimg_id, NULL, 0 ) ;
//	memset(ptr, 0, sizeof(img_meta_ptr) + 1280*720*3);
	memset(ptr, 0, sizeof(img_meta_ptr) + 560*420*3*3);
	img_meta_ptr = (img_meta *)ptr;
	img_meta_ptr->width = 560;
	img_meta_ptr->height = 420;
	img_meta_ptr->depth = 8;
	img_meta_ptr->channels = 3;
	shmdt( ptr ) ;

	shmsync_id = shmget(IPC_PRIVATE, sizeof(newsetting) + sizeof(GUI_data), IPC_CREAT|0600 ) ;
	if ( shmsync_id < 0 )
	{
		perror("get shmsync_id ipc_id error") ;
		exit(-1) ;
	}
	ptr = shmat(shmsync_id, NULL, 0 ) ;
	memset(ptr, 0, sizeof(newsetting) + sizeof(GUI_data));
	shmdt( ptr ) ;
}

int main(int argc, char *argv[])
{   
	int  sockfd, portno = PROTNO, frame_num = 0, n, status, coproc_pid = 0, delayread=2;
	long long start, end;
	char cascade_name[]=XML_LOCATION;
	uint32_t framesize, frame_totalsize, buffersize;
	char *buffer;
	char lebel;

	param[0]=CV_IMWRITE_JPEG_QUALITY;
        param[1]=95;//default(95) 0-100


	if (argc < 2) {
			printf("Usage: %s <odroid ip>\n",argv[0]);
			exit(1);
	}

	struct hostent *server = gethostbyname(argv[1]);
	if(!server){
		error("gethostbyname error\n");
	}
	sockfd = sock_init(server, portno);

	/*init shared memory for GUI and imgprocessing process*/
	shm_init();

	/*parent for GUI, child for image processing*/
	if((childpid = fork()) != 0){//parent
		gui_main(argc, argv);
		return 0;
	}

	/*initial shared memory ptr*/
	img_meta_ptr = (img_meta*)shmat(shmimg_id, NULL, 0);
	img_data1 = img_meta_ptr + 1;
	img_data2 = img_data1 + 560*420*3;
	img_data3 = img_data2 + 560*420*3;
	newsetting = (uint32_t*)shmat(shmsync_id, NULL, 0);
	GUI_data = (data_fmt *)(newsetting + 1);

	/*init data_fmt*/
	GUI_data->camera_device[0] = 1;
	GUI_data->camera_device[1] = 1;
	GUI_data->camera_device[2] = 1;

	GUI_data->proc_dev = NO_PROC;
	memcpy(&set_data, GUI_data, sizeof(data_fmt));

cout << "set_data.proc_dev="<<set_data.proc_dev<<endl;

	classifier=(CvHaarClassifierCascade*)cvLoad(cascade_name, 0, 0, 0);
	if(!classifier){
		fprintf(stderr,"ERROR: Could not load classifier cascade.");
		return -1;
	}
	facesMemStorage=cvCreateMemStorage(0);
	while(1){
		status = STATUS_FIRSTIMG;
		set_formate(sockfd);
		cout << "set formate success"<<endl;
		check_formate(sockfd);
		cout << "check_formate success  cam id="<<check_data.camera_device << " proc dev=" << check_data.proc_dev<<endl;
		char *blackimage = (char*)malloc(INIT_WIDTH*INIT_HEIGHT*3);
		frame_totalsize = 0;
		n = ewrite(sockfd,"...",3);//try		
		if(n <= 0)
			exit(-1);

		start = getSystemTime();
		while(1){
			//n = ewrite(sockfd,".",1);//try
              //  cout << "write... success n="<<n<<endl;

			n = eread(sockfd, &lebel, 1);//read S
			cout <<"read s success n=" <<n<<"lebel="<<lebel<<endl;
			if(n <= 0) exit(-1);
			if(lebel == 'S'){
				//free(buffer);
				break;
			}
			//set_formate(sockfd);
                	//cout << "set formate success"<<endl;
                	//check_formate(sockfd);
			switch(check_data.proc_dev){
					int totalface, i;
					CvPoint point1, point2;
					// char *blackimage;
				case NO_PROC:
					{
					for(int i=0;i<3;i++){
//  					for(int i=0;i<2;i++){
						if(check_data.camera_device[i]==1) {
	                                      		n = eread(sockfd, (char*) &framesize, 4);
                                        		cout << "read framesize="<<framesize<<"n=" << n<<endl;
                                        		if(n <= 0) exit(-1);
                                        		frame_totalsize += (uint32_t)framesize;
                                        		buffer = (char*)malloc(framesize*sizeof(char));
                                        		n = eread(sockfd, buffer, framesize);
                                        		cout << "read buffer n="<<n<<endl;
                                        		if(n <= 0) exit(-1);
                                        	//use vector constructor to convert array to vector
                                        		vector<uchar> encoded_data(buffer, buffer + framesize);
                                        		Mat decoded_data = imdecode(Mat(encoded_data),CV_LOAD_IMAGE_COLOR);
							cvtColor(decoded_data, decoded_data, COLOR_RGB2BGR);
							Mat resizeMat;
                                        		resize( decoded_data, resizeMat, Size(560,420) );
                                        		if(i==0)
								memcpy(img_data1, resizeMat.data, 560*420*3);
							else if(i==1)
								memcpy(img_data2, resizeMat.data, 560*420*3);
							else
								memcpy(img_data3, resizeMat.data, 560*420*3);
			
                        	                //decoded_data.release();
						//encoded_data.clear();
							free(buffer);
						}
				//		frame_num++;
					}


					}
					break;
				case REMOTE_PROC:
					{
//					for(int i=0;i<2;i++){
					for(int i=0;i<3;i++){
						if(check_data.camera_device[i]==1) {
							cout << "remote or no"<<endl;
							n = eread(sockfd, (char*) &framesize, 4);
							cout << "read framesize="<<framesize<<"n=" << n<<endl;	
							if(n <= 0) exit(-1);
							frame_totalsize += (uint32_t)framesize;
							buffer = (char*)malloc(framesize*sizeof(char));
							n = eread(sockfd, buffer, framesize);
							cout << "read buffer n="<<n<<endl;
							if(n <= 0) exit(-1);
							//use vector constructor to convert array to vector
							vector<uchar> encoded_data(buffer, buffer + framesize);
							Mat decoded_data = imdecode(Mat(encoded_data),CV_LOAD_IMAGE_COLOR);
							cout << "pc decode success"<<endl;
			//				imshow("pc decode", decoded_data);
			//				cvWaitKey(1);
							IplImage img = IplImage(decoded_data);
							IplImage* face_Iplptr;
							cout << "before doing fd"<<endl;
							face_Iplptr = facedetect(&img);
							cout << "after doing fd"<<endl;
							Mat face_detected(face_Iplptr, 0);
							//imshow("pc face",face_detected);
							//cvWaitKey(1);
							cvtColor(face_detected, face_detected, COLOR_RGB2BGR);
							Mat resizeMat;
							resize( face_detected, resizeMat, Size(560,420) );
							if(i==0)
								memcpy(img_data1, resizeMat.data, 560*420*3);
							else if(i==1)
								memcpy(img_data2, resizeMat.data, 560*420*3);
							else
								memcpy(img_data3, resizeMat.data, 560*420*3);
		
							cvReleaseImage(&face_Iplptr);
							free(buffer);
						}
				//		frame_num++;
					}
					}
					break;
				case LOCAL_PROC:
					{
					//for(int j=0;j<2;j++){
					for(int j=0;j<3;j++){
						if(check_data.camera_device[i]==1) {
							cout << "local"<<endl;
							// fprintf(stderr,LOCAL_PROC)
							memset(blackimage, 0, INIT_WIDTH*INIT_HEIGHT*3);
							IplImage *blackipl = cvCreateImage(cvSize(INIT_WIDTH, INIT_HEIGHT), IPL_DEPTH_8U, 3);
							blackipl->imageSize = INIT_WIDTH*INIT_HEIGHT*3;
							blackipl->imageData = blackimage;
							n = eread(sockfd, &totalface, 4);
							cout << "n="<<n<<"totalface=" <<totalface<<endl;
							if(n <= 0) exit(-1);
							for(i=0; i<totalface; i++){
								n = eread(sockfd,(char*)&(point1.x),4);
								if (n <= 0) exit(-1);
								n = eread(sockfd,(char*)&(point2.x),4);
								if (n <= 0) exit(-1);
								n = eread(sockfd,(char*)&(point1.y),4);
								if (n <= 0) exit(-1);
								n = eread(sockfd,(char*)&(point2.y),4);
								if (n <= 0) exit(-1);
								cvRectangle(blackipl, point1, point2, CV_RGB(255,0,0), 3, 8, 0);
							}
							Mat local(blackipl, 0);
							Mat resizeMat;
							resize( local, resizeMat, Size(560,420) );
							if(j==0)
								memcpy(img_data1, resizeMat.data, 560*420*3);
							else if(j==1)
								memcpy(img_data2, resizeMat.data, 560*420*3);
							else
								memcpy(img_data3, resizeMat.data, 560*420*3);
		
			//				memcpy(img_data, blackipl->imageData, INIT_WIDTH*INIT_HEIG*3);
							// free(blackimage);
							cvReleaseImage(&blackipl);
						}
					//		frame_num++;
					}
					}
					break;
				
				default:
//					cout << "default  cam id="<<check_data.camera_device << " proc dev=" << check_data.proc_dev<<endl;
					fprintf(stderr, "You should not see this!!! Something wrong!!!\n\n");
					break;			
			}

			if(*newsetting && status != STATUS_SETTING){
				n = ewrite(sockfd,"S",1);
				if(n <= 0) exit(-1);
				memcpy(&set_data, GUI_data, sizeof(data_fmt));
				xchg(newsetting, 0);
				status =STATUS_SETTING;
			}else{
				switch(status){
					case STATUS_FIRSTIMG:
						img_meta_ptr->width = 560;
						img_meta_ptr->height = 420;
					case STATUS_NORMAL:
						n = ewrite(sockfd,".",1);
						if(n <= 0) exit(-1);
						break;
					case STATUS_SETTING:
						break;
					default:
						fprintf(stderr,"Wrong status!!\n");
				}
			}
			frame_num++;
			if(frame_num >= 40){
				end = getSystemTime();
			//	frame_num = 0;
				img_meta_ptr->FPS = (frame_num*1000.0)/(end - start);
				img_meta_ptr->Stream_speed = frame_totalsize/(end - start);
				// PrintFrameMsg(20000.0/(end - start), frame_totalsize/(end - start), (char *)&check_data.frame_fmt);
				frame_totalsize = 0;
				frame_num = 0;
				start = getSystemTime();
			}
		}
	}


	close(sockfd);

	exit(0); 

}
IplImage *facedetect(IplImage* image_detect){
		// fprintf(stderr,"facedetect\n");
	int i;
	IplImage* frame=cvCreateImage(cvSize(image_detect->width, image_detect->height), IPL_DEPTH_8U, image_detect->nChannels);
	if(image_detect->origin==IPL_ORIGIN_TL){
		cvCopy(image_detect, frame, 0);    }
	else{
		cvFlip(image_detect, frame, 0);    }
	cvClearMemStorage(facesMemStorage);
	CvSeq* faces=cvHaarDetectObjects(frame, classifier, facesMemStorage, 1.1, 3, CV_HAAR_DO_CANNY_PRUNING, cvSize(min_face_width, min_face_height), cvSize(max_face_width,max_face_height));
	if(faces){
		// fprintf(stderr,"get %d faces\n", faces->total);
		for(i=0; i<faces->total; ++i){
			// Setup two points that define the extremes of the rectangle,
			// then draw it to the image
			CvPoint point1, point2;
			CvRect* rectangle = (CvRect*)cvGetSeqElem(faces, i);
			point1.x = rectangle->x;
			point2.x = rectangle->x + rectangle->width;
			point1.y = rectangle->y;
			point2.y = rectangle->y + rectangle->height;
			cvRectangle(frame, point1, point2, CV_RGB(255,0,0), 3, 8, 0);
		}
	}
//	cvReleaseImage(&image_detect);
	return frame;
}
