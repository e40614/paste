#include <netinet/in.h>
#include <netdb.h> 
#include <stdio.h>
#include "tools.h"
#include "odroid_status.h"
#include <signal.h>
#include <unistd.h>
#include <string.h>
#include <stdlib.h>

void send_status(int sockfd);
int cpu_usage();
int A15_pow();
int MEM_pow();
int GPU_pow();
int A7_pow();

int main(int argc, char *argv[])
{
	struct sockaddr_in serv_addr, cli_addr;	
	socklen_t clilen;
	int newsockfd, bindsockfd;
	signal(SIGCLD,SIG_IGN);
#ifndef PC
	system("echo 1 > /sys/bus/i2c/drivers/INA231/3-0045/enable");
	system("echo 1 > /sys/bus/i2c/drivers/INA231/3-0040/enable");
	system("echo 1 > /sys/bus/i2c/drivers/INA231/3-0041/enable");
	system("echo 1 > /sys/bus/i2c/drivers/INA231/3-0044/enable");
#endif
	bindsockfd = socket(AF_INET, SOCK_STREAM, 0);
	if (bindsockfd < 0) 
		fprintf(stderr,"ERROR opening socket\n");
	bzero((char *) &serv_addr, sizeof(serv_addr));
	serv_addr.sin_family = AF_INET;
	serv_addr.sin_addr.s_addr = INADDR_ANY;
	serv_addr.sin_port = htons(STATUS_PORT);
	if (bind(bindsockfd, (struct sockaddr *) &serv_addr, sizeof(serv_addr)) < 0) 
		fprintf(stderr,"ERROR on binding\n");
	listen(bindsockfd,5);
	fflush(stdout);

	while(1){
		clilen = sizeof(cli_addr);
		newsockfd = accept(bindsockfd, 
				(struct sockaddr *) &cli_addr, 
				&clilen);
		if (newsockfd < 0) 
			fprintf(stderr,"ERROR on accept");
		if(!fork()){//child
			close(bindsockfd);
			send_status(newsockfd);
		}else{//parent
			close(newsockfd);
		}
	}
}

void send_status(int sockfd){
	int status_type, n, ret;
	while(1){
		n = eread(sockfd, &status_type, 4);
		if(n <= 0) exit(0);
		switch(status_type){
			case STATUS_CPU:
				ret = cpu_usage();
				break;
			case STATUS_POWER_A15:
				ret = A15_pow();
				break;		
			case STATUS_POWER_MEM:
				ret = MEM_pow();
				break;	
			case STATUS_POWER_GPU:
				ret = GPU_pow();
				break;	
			case STATUS_POWER_A7:
				ret = A7_pow();
				break;	

			default:
				ret = -1;
				n = ewrite(sockfd, &ret, 4);
				if(n <= 0) exit(0);				
				fprintf(stderr,"You should not see this!\n");
				break;
		}
		n = ewrite(sockfd, &ret, 4);
		if(n <= 0) exit(0);
	}


}

int cpu_usage(){
	static long double a[4], b[4], loadavg ,first = 1;
	FILE *fp;
	fp = fopen("/proc/stat","r");
	if(fscanf(fp,"%*s %Lf %Lf %Lf %Lf",&b[0],&b[1],&b[2],&b[3]) == EOF){
		fprintf(stderr,"fscanf read EOF\n");
	}
	fclose(fp);
	loadavg = ((b[0]+b[1]+b[2]) - (a[0]+a[1]+a[2])) / ((b[0]+b[1]+b[2]+b[3]) - (a[0]+a[1]+a[2]+a[3]));
	a[0] = b[0];
	a[1] = b[1];
	a[2] = b[2];
	a[3] = b[3];
	if(first){
		first = 0;
		return 0;
	}
	return (int)(loadavg*SCALE);
}

int A15_pow(){
	long double retVal;
	FILE *fp;
	fp = fopen("/sys/bus/i2c/drivers/INA231/3-0040/sensor_W","r");
	if(fp == NULL)
		return -1;
	if(fscanf(fp,"%Lf",&retVal) == EOF){
		fprintf(stderr,"fscanf read EOF\n");
	}
	fclose(fp);
	return (int)(retVal*SCALE);
}

int MEM_pow(){
	long double retVal;
	FILE *fp;
	fp = fopen("/sys/bus/i2c/drivers/INA231/3-0041/sensor_W","r");
	if(fp == NULL)
		return -1;
	if(fscanf(fp,"%Lf",&retVal) == EOF){
		fprintf(stderr,"fscanf read EOF\n");
	}
	fclose(fp);
	return (int)(retVal*SCALE);
}

int GPU_pow(){
	long double retVal;
	FILE *fp;
	fp = fopen("/sys/bus/i2c/drivers/INA231/3-0044/sensor_W","r");
	if(fp == NULL)
		return -1;
	if(fscanf(fp,"%Lf",&retVal) == EOF){
		fprintf(stderr,"fscanf read EOF\n");
	}
	fclose(fp);
	return (int)(retVal*SCALE);
}

int A7_pow(){
	long double retVal;
	FILE *fp;
	fp = fopen("/sys/bus/i2c/drivers/INA231/3-0045/sensor_W","r");
	if(fp == NULL)
		return -1;
	if(fscanf(fp,"%Lf",&retVal) == EOF){
		fprintf(stderr,"fscanf read EOF\n");
	}
	fclose(fp);
	return (int)(retVal*SCALE);
}
