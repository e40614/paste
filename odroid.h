#include <stdlib.h>
#include <stdint.h>


#define CLEAR(x) memset(&(x), 0, sizeof(x))
int capture_and_send_image(int sockfd);
void error(const char *msg);
void ori_ctrlC(int sig);
IplImage *facedetect(IplImage* image_detect, int sockfd);


struct buffer {
	void   *start;
	size_t  length;
};
