#include <errno.h>
#include <fcntl.h>
#include <linux/videodev2.h>
#include <stdio.h>
#include <string.h>
#include <sys/ioctl.h>
#include <sys/mman.h>
#include <unistd.h>
#include <opencv2/highgui/highgui.hpp>
//#include "cv.h"
#include <netinet/in.h>
#include <netdb.h> 
#include <signal.h>
#include <getopt.h> 
#include "odroid.h"
#include "tools.h"

#include <iostream>
#include <highgui.h>
#include <cv.h>
#include <vector>
#include <sys/time.h>

#include "I2Cdev.h"
#include "MPU6050.h"

using namespace std;
using namespace cv;

MPU6050 accelgyro;

int	is_sending = 0;

struct itimerval value, ovalue, value2;

int sleep_utime = 200000;//control FPS
int sleep_control = 1;

struct sockaddr_in serv_addr;
int bindsockfd = -1;

sensor_stat	sensor_x;
sensor_stat	sensor_y;
//sensor_stat	sensor_z;

int		THR = 3000;

data_fmt	set_data;
data_fmt	check_data;

CvMemStorage* facesMemStorage;
CvHaarClassifierCascade* classifier;

vector<uchar> buff1,buff2,buff3;//buffer for coding
vector<int> param = vector<int>(2);

int capture_first=1;

//CvCapture *capture1=NULL, *capture2=NULL, *capture3=NULL;
VideoCapture capture1, capture2, capture3;

//IplImage *frame1,*frame2,*frame3;
Mat frame1, frame2, frame3;
IplImage frame1_ipl, frame2_ipl, frame3_ipl;
void fps_handler(int a)
{
	cout << "fps handler" <<endl;
	int16_t ax, ay, az;
        int16_t gx, gy, gz;
        accelgyro.getMotion6(&ax, &ay, &az, &gx, &gy, &gz);
	if( (ax<(-1*THR)) || ax >THR )
		sleep_control = 0;
	else
		sleep_control = 1;
	signal(SIGVTALRM, fps_handler);
}

void state_handler(int a)
{
	cout << "state handler"<<endl;
	int16_t ax, ay, az;
	int16_t gx, gy, gz;
	if(is_sending){
		//cout << "IN TIMER IS SENDING = 1~~~~~~~" <<endl;
		signal(SIGALRM, state_handler);
		return;
	}
//	else 
  //              cout << "IN TIMER IS SENDING = 0~~~~~~" <<endl;

//	cout << "IN ALAM~~~~~~" <<endl;
//	while(1);
	accelgyro.getMotion6(&ax, &ay, &az, &gx, &gy, &gz);
	cout << "ax" << ax <<"ay" <<ay<<"az"<<az<<endl;
//while(1);
	if(ax < (-1* THR)) {
		switch(sensor_x)
		{
                        case f:
                                sensor_x = f;
                                break;
                        case b:
                                sensor_x = f;
                                break;
                        default:
				cerr << "sensor_x err" << endl;
                                exit(1);
                                break;
		}
	}
        else if(ax >THR ) {
                switch(sensor_x)
                {
                        case f:
                                sensor_x = b;
                                break;
                        case b:
                                sensor_x = b;
				break;
                        default:
                                cerr << "sensor_x err" << endl;
                                exit(1);
                                break;
                }
        }
/*
        if(ay > THR) {
                switch(sensor_y)
                {
                        case f2:
                                sensor_y = f2;
                                break;
                        case f1:
                                sensor_y = f2;
                                break;
                        case stop:
                                sensor_y = f1;
                                break;
                        case b1:
                                sensor_y = stop;
                                break;
                        case b2:
                                sensor_y = b1;
                                break;
                        default:
                                cerr << "sensor_y err" << endl;
                                exit(1);
                                break;
                }
        }
        else if(ay < (-1*THR) ) {
                switch(sensor_y)
                {
                        case f2:
                                sensor_y = f1;
                                break;
                        case f1:
                                sensor_y = stop;
                                break;
                        case stop:
                                sensor_y = b1;
                                break;
                        case b1:
                                sensor_y = b2;
                                break;
                        case b2:
                                sensor_y = b2;
                                break;
                        default:
                                cerr << "sensor_y err" << endl;
                                exit(1);
                                break;
                }
        }
*/
//	sleep_control = 0;


	if(sensor_x == 0) {//f
		set_data.camera_device[0] = 1;
                set_data.camera_device[1] = 0;
                set_data.camera_device[2] = 0;
/*
		if(sensor_y < 2) { //f1 or f2
			set_data.camera_device[0] = check_data.camera_device[0] = 1;
			set_data.camera_device[1] = check_data.camera_device[1] = 1;
			set_data.camera_device[2] = check_data.camera_device[2] = 0;
		}
		else if(sensor_y > 2) { //b1 or b2
                        set_data.camera_device[0] = check_data.camera_device[0] = 1;
                        set_data.camera_device[1] = check_data.camera_device[1] = 0;
                        set_data.camera_device[2] = check_data.camera_device[2] = 1;
                }
		else { //stop
                        set_data.camera_device[0] = check_data.camera_device[0] = 1;
                        set_data.camera_device[1] = check_data.camera_device[1] = 0;
                        set_data.camera_device[2] = check_data.camera_device[2] = 0;
                }
*/
	}
	else{//b
                set_data.camera_device[0] = 0;
                set_data.camera_device[1] = 1;
                set_data.camera_device[2] = 1;

/*
                if(sensor_y < 2) { //f1 or f2
                        set_data.camera_device[0] = check_data.camera_device[0] = 0;
                        set_data.camera_device[1] = check_data.camera_device[1] = 1;
                        set_data.camera_device[2] = check_data.camera_device[2] = 1;//0
                }
                else if(sensor_y > 2) { //b1 or b2
                        set_data.camera_device[0] = check_data.camera_device[0] = 0;
                        set_data.camera_device[1] = check_data.camera_device[1] = 1;//0
                        set_data.camera_device[2] = check_data.camera_device[2] = 1;
                }
                else { //stop
			sleep_control = 1;
                        set_data.camera_device[0] = check_data.camera_device[0] = 0;
                        set_data.camera_device[1] = check_data.camera_device[1] = 0;
                        set_data.camera_device[2] = check_data.camera_device[2] = 0;
                }
*/
        }

//                        set_data.camera_device[0] = 0;
//                        set_data.camera_device[1] = 1;
//                        set_data.camera_device[2] = 1;

	is_sending = 1;
	signal(SIGALRM, state_handler);
//	alarm(1);
}

void get_formate(int sockfd){
	eread(sockfd, set_data.camera_device, sizeof(int)*3);
	eread(sockfd, &(set_data.proc_dev),   sizeof(set_data.proc_dev));
}

void ret_formate(int sockfd){
	ewrite(sockfd, check_data.camera_device, sizeof(int)*3);
	ewrite(sockfd, &(check_data.proc_dev),   sizeof(check_data.proc_dev));
}

int main(int argc, char *argv[])
{

	sensor_x = f;
	sensor_y = f;


	param[0]=CV_IMWRITE_JPEG_QUALITY;
	param[1]=95;//default(95) 0-100

	
	int  newsockfd, childpid;
	socklen_t clilen;
	struct sockaddr_in serv_addr, cli_addr;


	classifier=(CvHaarClassifierCascade*)cvLoad(XML_LOCATION, 0, 0, 0);
	if(!classifier){
		fprintf(stderr,"ERROR: Could not load classifier cascade.");
		return -1;
	}
	facesMemStorage=cvCreateMemStorage(0);

	signal(SIGINT, ori_ctrlC);
	signal(SIGPIPE,SIG_IGN);
	signal(SIGCLD,SIG_IGN);
	bindsockfd = socket(AF_INET, SOCK_STREAM, 0);
	if (bindsockfd < 0) 
		error("ERROR opening socket");
	bzero((char *) &serv_addr, sizeof(serv_addr));
	serv_addr.sin_family = AF_INET;
	serv_addr.sin_addr.s_addr = INADDR_ANY;
	serv_addr.sin_port = htons(PROTNO);
	int reuseaddr = 1;
	socklen_t reuseaddr_len;
	reuseaddr_len = sizeof(reuseaddr);
	setsockopt(bindsockfd, SOL_SOCKET, SO_REUSEADDR, &reuseaddr, reuseaddr_len);
	if (bind(bindsockfd, (struct sockaddr *) &serv_addr, sizeof(serv_addr)) < 0) 
		error("ERROR on binding");
	listen(bindsockfd,5);
	fflush(stdout);

//	cout << "socket init success" <<endl;
	while(1){

		clilen = sizeof(cli_addr);
		newsockfd = accept(bindsockfd, 
				(struct sockaddr *) &cli_addr, 
				&clilen);
		if (newsockfd < 0) 
			error("ERROR on accept");
		if((childpid = fork()) < 0)
			error("server:fork error");
		else if(childpid == 0){//child
			close(bindsockfd);
/*
		        struct itimerval t;
	       		t.it_interval.tv_usec = 500;
		       	t.it_interval.tv_sec = 0;
		        t.it_value.tv_usec = 500;
		        t.it_value.tv_sec = 0;
		        if( setitimer( ITIMER_REAL, &t, NULL) < 0 ){
	        	        cerr<<"settimer error."<<endl;
	                	return -1;
		        }
*/
			signal(SIGALRM, state_handler);
			signal(SIGVTALRM, fps_handler);
			value.it_value.tv_sec = 0;
			value.it_value.tv_usec = 800000;
			value.it_interval.tv_sec = 0;
			value.it_interval.tv_usec = 800000;
			setitimer(ITIMER_REAL, &value, &ovalue); //(2)
			value2.it_value.tv_sec = 1;
			value2.it_value.tv_usec = 0;
			value2.it_interval.tv_sec = 1;
			value2.it_interval.tv_usec = 0;
			setitimer(ITIMER_VIRTUAL, &value2, &ovalue);
//			signal(SIGALRM, SIG_IGN);
//			cout << "set sig_alarm" <<endl;
			signal(SIGINT, SIG_DFL);
			while(1){
				get_formate(newsockfd);
//				cout << "after get formate"<<endl;
		//		cout << "set_data formate cam id=" << set_data.camera_device << " proc dev=" << set_data.proc_dev <<endl;
//				fd = open(set_data.camera_device, O_RDWR);
				if(capture_first){
//					capture1 = cvCaptureFromCAM(0);//check_data.camera_device
//					capture2 = cvCaptureFromCAM(1);
//					capture3 = cvCaptureFromCAM(2);
					capture_first=0;
//				cout <<"after create capture"<<endl;
				}
				memcpy(check_data.camera_device, set_data.camera_device, sizeof(int)*3);
				//check_data.camera_device = set_data.camera_device;
				check_data.proc_dev = set_data.proc_dev;
	//			cout << "check_data formate cam id=" << check_data.camera_device << " proc dev=" << check_data.proc_dev <<endl;	
//				if(print_caps(fd))
//					return 1;

				ret_formate(newsockfd);
				fprintf(stderr,"retrun formate success\n");

				capture_and_send_image(newsockfd);

			}
		}else{
			close(newsockfd);
		}

	}
	return 0;
}

void ori_ctrlC(int sig){ // can be called asynchronously
	close(bindsockfd);	
//	cvReleaseCapture(&capture1);
  //      cvReleaseCapture(&capture2);
    //    cvReleaseCapture(&capture3);
	for(int i=0;i<3;i++) {
		switch(i)
		{
			case 0:
				if(capture1.isOpened())
					capture1.release();
				break;
			case 1:
				if(capture2.isOpened())
					capture2.release();
				break;
			case 2:
				if(capture3.isOpened())
					capture3.release();
				break;
		}

	}


	exit(0);
}
int capture_and_send_image(int sockfd)
{
	char rec;

	while(1){

		eread(sockfd,&rec,1);
//		cout << "read s success rec=" << rec << endl;
//		if(n <= 0) ctrlC(0);
		ewrite(sockfd,&rec,1);
//		cout << "write s success" <<endl;
//		if(n <= 0) ctrlC(0);
		if(rec == 'S'){
//			cout << "fuck"<<endl;
			return 0;
		}
		ret_formate(sockfd);
//		CLEAR(buf);
//must release before open
		for(int i=0;i<3;i++) {
			if(check_data.camera_device[i]==0){
				switch(i)
				{
					case 0:
						if(capture1.isOpened())
							capture1.release();
						break;
                                        case 1:
                                                if(capture2.isOpened())
                                                        capture2.release();
                                                break;
                                        case 2:
                                                if(capture3.isOpened())
                                                        capture3.release();
                                                break;
				}
				
			}
		}
		if(check_data.camera_device[0]==1){
//			capture1 = cvCaptureFromCAM(0);
			if(!capture1.isOpened())
				capture1.open(0);
			capture1 >> frame1;
			//frame1 = cvQueryFrame(capture1);
		}
                if(check_data.camera_device[1]==1){
//			capture1 = cvCaptureFromCAM(1);
//                        frame2 = cvQueryFrame(capture2);
			if(!capture2.isOpened())
                        	capture2.open(1);
                        capture2 >> frame2;
                }
                if(check_data.camera_device[2]==1){
//			capture1 = cvCaptureFromCAM(2);
//                        frame3 = cvQueryFrame(capture3);
			if(!capture3.isOpened())
                        	capture3.open(2);
                        capture3 >> frame3;
                }


//		cout << "query success"<<endl;
		usleep(sleep_utime*sleep_control);
//		cout <<"after sleep"<<" sleep_control="<<sleep_control<<endl;
//		cout << "camera device"<<check_data.camera_device[0]<<check_data.camera_device[1]<<check_data.camera_device[2]<<endl;
		switch(set_data.proc_dev){
			case LOCAL_PROC:
				{
//				cout << "porc=lovali"<<endl;
				IplImage *cpimgrgb=NULL;
				if(check_data.camera_device[0]==1){
					frame1_ipl = IplImage(frame1);
					cpimgrgb=facedetect(&frame1_ipl, sockfd);
				}
                                if(check_data.camera_device[1]==1){
                                        frame2_ipl = IplImage(frame2);
                                        cpimgrgb=facedetect(&frame2_ipl, sockfd);
                                }
                                if(check_data.camera_device[2]==1){
                                        frame3_ipl = IplImage(frame3);
                                        cpimgrgb=facedetect(&frame3_ipl, sockfd);
                                }

//				if(cpimgrgb!=NULL) cout << "local do something" <<endl;
				//cvReleaseImage(&cpimgrgb);
				}
				break;
			default:
				{
				uchar* vector_data;
				int n,buff_size;
//				IplImage *flip1_ptr, *flip2_ptr, *flip3_ptr;
//				cout<<"proc remote=1 no=3  val= "<<set_data.proc_dev<<endl;
				if(check_data.camera_device[0]==1){
//					frame1_ipl = IplImage(frame1);
//					cvFlip(&frame1_ipl, flip1_ptr, 0);
				//	flip(frame1, frame1, 1);
					imencode(".jpg", frame1, buff1,param);
				}
				if(check_data.camera_device[1]==1){
                                  //      flip(frame2, frame2, 1);                                        
					imencode(".jpg", frame2, buff2,param);
				}
				if(check_data.camera_device[2]==1){
                                    //    flip(frame3, frame3, 1);
                                        imencode(".jpg", frame3, buff3,param);
				}
				if(check_data.camera_device[0]==1) {
					buff_size=buff1.size();
					ewrite(sockfd,(char*)&buff_size,4);
//					if (n <= 0) ctrlC(0);
					vector_data = &buff1[0];
					n=ewrite(sockfd,vector_data ,buff1.size());
				}
				if(check_data.camera_device[1]==1) {
					buff_size=buff2.size();
					ewrite(sockfd,(char*)&buff_size,4);
	//                              if (n <= 0) ctrlC(0);
					vector_data = &buff2[0];
					n=ewrite(sockfd,vector_data ,buff2.size());
				}
				if(check_data.camera_device[2]==1) {
					buff_size=buff3.size();
					ewrite(sockfd,(char*)&buff_size,4);
	//                              if (n <= 0) ctrlC(0);
					vector_data = &buff3[0];
					n=ewrite(sockfd,vector_data ,buff3.size());
				}

//				cout <<"write buff size="<<buff1.size()<<"n="<<n<<endl;
//				if (n <= 0)	ctrlC(0);
				}
				break;
		}
		memcpy(check_data.camera_device, set_data.camera_device, sizeof(int)*3);
		is_sending = 0;
/*
		Mat mat(frame, 0);
		imshow("odroid cap", mat);
cout <<"row " << mat.rows << " col " << mat.cols << endl;
				if(cvWaitKey(1)>=0){
			break;
		}
*/

		
	
	}
	return 0;
}

IplImage *facedetect(IplImage* image_detect, int sockfd){
		// fprintf(stderr,"facedetect\n");
	int i, position;
	IplImage* frame=cvCreateImage(cvSize(image_detect->width, image_detect->height), IPL_DEPTH_8U, image_detect->nChannels);
//cout << "create succcess" << endl;
	//imshow("odroid cam", Mat(frame));
	//cvWaitKey(1);
	if(image_detect->origin==IPL_ORIGIN_TL){
		cout << "copy" <<endl;
		cvCopy(image_detect, frame, 0);    
	}
	else{
		cout << "flip" <<endl;
		cvFlip(image_detect, frame, 0);    }
//cout <<"flip success"<<endl;
	cvClearMemStorage(facesMemStorage);
	CvSeq* faces=cvHaarDetectObjects(frame, classifier, facesMemStorage, 1.1, 3, CV_HAAR_DO_CANNY_PRUNING, cvSize(min_face_width, min_face_height), cvSize(max_face_width,max_face_height));
//cout <<"detect success"<<endl;
//cout << "face:"<<faces->total<<endl;
	if(faces){
		ewrite(sockfd,(char*)&(faces->total),4);
		//if (n <= 0) ctrlC(0);
		// fprintf(stderr,"get %d faces\n", faces->total);
		for(i=0; i<faces->total; ++i){
			// Setup two points that define the extremes of the rectangle,
			// then draw it to the image
			// CvPoint point1, point2;

			CvRect* rectangle = (CvRect*)cvGetSeqElem(faces, i);
			position = rectangle->x;
			ewrite(sockfd,(char*)&position,4);
		//	if (n <= 0) ctrlC(0);
			position = rectangle->x + rectangle->width;
			ewrite(sockfd,(char*)&position,4);
		//	if (n <= 0) ctrlC(0);
			position = rectangle->y;
			ewrite(sockfd,(char*)&position,4);
		//	if (n <= 0) ctrlC(0);
			position = rectangle->y + rectangle->height;
			ewrite(sockfd,(char*)&position,4);

		//	if (n <= 0) ctrlC(0);
		}
	}else{
		int zero = 0;
		ewrite(sockfd,(char*)&zero,4);
	//	if (n <= 0) ctrlC(0);
	}
	//cvReleaseImage(&image_detect);
	return frame;
}
