#include <stdint.h>
#define LIGHT_RED "\033[1;31m"
#define YELLOW "\033[1;33m"
#define CLOSE_COLOR "\033[0m"


#define STATUS_NORMAL	0
#define STATUS_SETTING	1
#define STATUS_FIRSTIMG 2

long long getSystemTime();
void PrintFrameMsg(double fps,long long speed, char *fourcc);
IplImage *facedetect(IplImage* image_detect);
